# Redis Developer Fundamentals Code Samples#


This repository contains the code samples demonstrated in the Redis Developer Fundamentals brown bag.

##Samples##

#Before you begin#

1. Make sure SQL Server is installed on your machine.
2. Download the backup of the [AdventureWorks2014](https://msftdbprodsamples.codeplex.com/releases/view/125550) database.
3. Restore the backup to your SQL Server instance.
4. Clone this repository.
5. Edit the **awdb** connection string in the web config to point to the AdventureWorks2014 database on your machine.
6. Make sure Redis is installed and running. You can get the Windows Installer [here](https://bitbucket.org/farooq_mahmud/redisdeveloperfundamentals).

#String Demo#

1. Start the **AdventureWorksApi** project.
2. Open Postman or a similar tool.
3. Issue a **GET** to http://{root url}/**api/employees/adventure-works/diane1**
4. When you get the response, verify Redis has cached the employee by running the following in the Redis CLI: **GET"employees:adventure-works\\diane1"**

#Sorted Set Demo#
1. Start the **AdventureWorksApi** project.
2. Open Postman or a similar tool.
3. Issue a **GET to http://{{root url}/**api/salesTerritories/top/5**
4. Observe the response has five territories ordered in descending order by year to date sales.
5. When you get the response, verify Redis has cached the sales information by running the following in the Redis CLI: **ZREVRANGEBYSCORE topSalesTerritories +inf -inf withscores**
6. Observe Redis has cached all twenty sales territories in descending order by year to date sales.

#Hash Demo#
1. Start the **AdventureWorksApi** project.
2. Open Postman or a similar tool.
3. Issue a **POST** to http://{{root url}/**api/workorders/prime**
4. After a few seconds you will get an HTTP 204 (No response)
5. Issue a **GET** to http://{{root url}/**api/workorders/3**
6. Verify the work order is returned in the response.
7. Verify Redis has cached the employee by running the following in the Redis CLI: **HGETALL workOrders:3**
8. Verify Redis has cached the 70K+ work orders by running the following in the Redis CLI: **INFO Keyspace** and observing the number of keys is 72,591.

#HyperLogLog Demo#
1. Start the **BuggyApp** **AdventureWorksApi** and project.
2. Wait for the app to finish.
3. Execute the following in the Redis CLI: **PFCOUNT buggyApp:events:errors** and observe the returned value is close to 1 million.
4. Browse to http://{{root url}/**index/redisInfo** and observe the memory usage (**used_memory_human** statistic) is pretty low for a set of 1 million GUID's. 

### Questions or feedback ###

Contact Farooq Mahmud via Slalom email.