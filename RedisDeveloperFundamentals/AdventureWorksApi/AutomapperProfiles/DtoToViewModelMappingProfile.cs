﻿using AdventureWorksApi.Dto;
using AdventureWorksApi.Models;
using AutoMapper;

namespace AdventureWorksApi.AutomapperProfiles
{
    public sealed class DtoToViewModelMappingProfile : Profile
    {
        public DtoToViewModelMappingProfile() : base("DtoToViewModelMappingProfile")
        {
            CreateMap<Employee, EmployeeViewModel>();
            CreateMap<ProductReviewViewModel, ProductReview>();
            CreateMap<SalesTerritory, SalesTerritoryViewModel>();
            CreateMap<WorkOrder, WorkOrderViewModel>();
        }
    }
}