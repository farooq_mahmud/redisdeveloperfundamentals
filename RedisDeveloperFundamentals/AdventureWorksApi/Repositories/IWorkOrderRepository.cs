﻿using System.Threading.Tasks;
using AdventureWorksApi.Dto;

namespace AdventureWorksApi.Repositories
{
    public interface IWorkOrderRepository
    {
        Task<WorkOrder> GetWorkOrderAsync(int workOrderId);
        Task PrimeCacheAsync();
    }
}