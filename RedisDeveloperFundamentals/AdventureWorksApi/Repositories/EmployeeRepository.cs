﻿using System.Data.Entity;
using System.Threading.Tasks;
using AdventureWorksApi.Data;
using AdventureWorksApi.Dto;
using Serilog;
using StackExchange.Redis.Extensions.Core;

namespace AdventureWorksApi.Repositories
{
    public sealed class EmployeeRepository : AdventureWorksRepository, IEmployeeRepository
    {
        public EmployeeRepository(
            AdventureWorksContext dataContext,
            ICacheClient cacheClient,
            ILogger logger) : base(dataContext, cacheClient, logger)
        {
        }

        public async Task<Employee> GetEmployeeAsync(string loginName)
        {
            var cacheKey = $"employees:{loginName}";

            if (await CacheClient.ExistsAsync(cacheKey))
            {
                Logger.Verbose("Cache key {key} found in cache - bypassing db lookup.", cacheKey);

                return await CacheClient.GetAsync<Employee>(cacheKey);
            }

            Logger.Verbose("Cache key {key} not found in cache - performing db lookup.", cacheKey);

            var employee = await DataContext.Employees.SingleAsync(e => e.LoginId == loginName);

            Logger.Verbose("Adding cache key {key} to cache.", cacheKey);

            await CacheClient.AddAsync(cacheKey, employee);
            return employee;
        }
    }
}