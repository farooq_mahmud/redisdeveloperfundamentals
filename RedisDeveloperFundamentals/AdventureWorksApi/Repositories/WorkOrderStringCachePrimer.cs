﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdventureWorksApi.Dto;
using AdventureWorksApi.Extensions;
using StackExchange.Redis.Extensions.Core;

namespace AdventureWorksApi.Repositories
{
    public sealed class WorkOrderStringCachePrimer : CachePrimer, ICachePrimer<IEnumerable<WorkOrder>>
    {
        public WorkOrderStringCachePrimer(ICacheClient cacheClient) : base(cacheClient)
        {
        }

        public async Task PrimeAsync(IEnumerable<WorkOrder> source)
        {
            var hashSetKey = "workOrders";
            var tuples = source.ToTuples(o => $"{hashSetKey}:{o.WorkOrderId}").ToList();
            await CacheClient.AddAllAsync(tuples);
        }
    }
}