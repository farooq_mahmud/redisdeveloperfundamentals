﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AdventureWorksApi.Data;
using AdventureWorksApi.Dto;
using AdventureWorksApi.Extensions;
using Serilog;
using StackExchange.Redis;
using StackExchange.Redis.Extensions.Core;

namespace AdventureWorksApi.Repositories
{
    public sealed class SalesTerritoryRepository : AdventureWorksRepository, ISalesTerritoryRepository
    {
        public SalesTerritoryRepository(
            AdventureWorksContext dataContext,
            ICacheClient cacheClient,
            ILogger logger) : base(dataContext, cacheClient, logger)
        {
        }

        public async Task<IEnumerable<SalesTerritory>> GetTopSalesTerritoriesAsync(int count)
        {
            var cacheKey = "topSalesTerritories";
            var salesTerritories = new List<SalesTerritory>();

            if (await CacheClient.ExistsAsync(cacheKey))
            {
                Logger.Debug("Found {key} in cache - bypassing db lookup.", cacheKey);

                var sortedSetEntries = CacheClient.Database.SortedSetRangeByRankWithScores(
                    cacheKey,
                    order: Order.Descending,
                    stop: count - 1);

                salesTerritories = CacheClient.ToEnumerable<SalesTerritory>(sortedSetEntries).ToList();

                Logger.Debug("Returning {count} territories from cache.", salesTerritories.Count);

                return salesTerritories;
            }

            Logger.Debug("Did not find {key} in cache - loading from db.", cacheKey);

            salesTerritories = await DataContext.SalesTerritories.ToListAsync();

            var sortedSet = CacheClient.ToSortedSet(salesTerritories, t => (double) t.SalesYtd);

            Logger.Debug("Adding {count} territories to cache {key}.", sortedSet.Length, cacheKey);

            await CacheClient.Database.SortedSetAddAsync(cacheKey, sortedSet);

            return salesTerritories.OrderByDescending(t => t.SalesYtd).Take(count);
        }
    }
}