﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AdventureWorksApi.Dto;

namespace AdventureWorksApi.Repositories
{
    public interface IProductReviewRepository
    {
        Task AddProductReviewsAsync(IEnumerable<ProductReview> productReviews);
    }
}