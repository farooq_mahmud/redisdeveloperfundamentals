﻿using System.Threading.Tasks;

namespace AdventureWorksApi.Repositories
{
    public interface ICachePrimer<in T> where T : class
    {
        Task PrimeAsync(T source);
    }
}