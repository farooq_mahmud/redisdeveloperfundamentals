﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AdventureWorksApi.Dto;
using StackExchange.Redis.Extensions.Core;

namespace AdventureWorksApi.Repositories
{
    public sealed class WorkOrderHashCachePrimer : CachePrimer, ICachePrimer<IEnumerable<WorkOrder>>
    {
        public WorkOrderHashCachePrimer(ICacheClient cacheClient) :  base(cacheClient)
        {
            
        }
        public async Task PrimeAsync(IEnumerable<WorkOrder> source)
        {
            var hashSetKey = "workOrders";
            
            foreach (var workOrder in source)
            {
                var hashItemKey = $"{hashSetKey}:{workOrder.WorkOrderId}";

                var dictionary = new Dictionary<string, object>
                {
                    {"productId", workOrder.ProductId},
                    {"orderQty", workOrder.OrderQty},
                    {"stockedQty", workOrder.StockedQty},
                    {"scrappedQty", workOrder.ScrappedQty},
                    {"startDate", workOrder.StartDate},
                    {"endDate", workOrder.EndDate}
                };

                await CacheClient.HashSetAsync(hashItemKey, dictionary);
            }
        }
    }
}