﻿using AdventureWorksApi.Data;
using Serilog;
using StackExchange.Redis.Extensions.Core;

namespace AdventureWorksApi.Repositories
{
    public abstract class AdventureWorksRepository
    {
        protected AdventureWorksRepository(
            AdventureWorksContext dataContext,
            ICacheClient cacheClient,
            ILogger logger)
        {
            DataContext = dataContext;
            CacheClient = cacheClient;
            Logger = logger;
        }

        protected AdventureWorksContext DataContext { get; private set; }
        public ICacheClient CacheClient { get; private set; }
        public ILogger Logger { get; private set; }
    }
}