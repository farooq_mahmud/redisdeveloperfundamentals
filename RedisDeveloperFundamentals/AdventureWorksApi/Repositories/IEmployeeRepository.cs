﻿using System.Threading.Tasks;
using AdventureWorksApi.Dto;

namespace AdventureWorksApi.Repositories
{
    public interface IEmployeeRepository
    {
        Task<Employee> GetEmployeeAsync(string loginName);
    }
}