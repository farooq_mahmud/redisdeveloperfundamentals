﻿using StackExchange.Redis.Extensions.Core;

namespace AdventureWorksApi.Repositories
{
    public abstract class CachePrimer
    {
        protected ICacheClient CacheClient { get; private set; }

        protected CachePrimer(ICacheClient cacheClient)
        {
            CacheClient = cacheClient;
        }
    }
}