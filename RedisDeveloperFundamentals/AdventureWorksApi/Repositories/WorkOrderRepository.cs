﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using AdventureWorksApi.Data;
using AdventureWorksApi.Dto;
using Serilog;
using StackExchange.Redis.Extensions.Core;

namespace AdventureWorksApi.Repositories
{
    public sealed class WorkOrderRepository : AdventureWorksRepository, IWorkOrderRepository
    {
        private readonly ICachePrimer<IEnumerable<WorkOrder>> _cachePrimer;
        private readonly Func<ICacheClient, string, WorkOrder> _workOrderFactory;

        public WorkOrderRepository(
            AdventureWorksContext dataContext,
            ICacheClient cacheClient,
            ILogger logger,
            ICachePrimer<IEnumerable<WorkOrder>> cachePrimer,
            Func<ICacheClient, string, WorkOrder> workOrderFactory) : base(dataContext, cacheClient, logger)
        {
            _cachePrimer = cachePrimer;
            _workOrderFactory = workOrderFactory;
        }

        public async Task<WorkOrder> GetWorkOrderAsync(int workOrderId)
        {
            var cacheKey = $"workOrders:{workOrderId}";
            var workOrder = _workOrderFactory(CacheClient, cacheKey);
            workOrder.WorkOrderId = workOrderId;
            return workOrder;
        }

        public async Task PrimeCacheAsync()
        {
            Logger.Verbose("Getting work orders from db.");

            var workOrders = await DataContext.WorkOrders.ToListAsync();

            Logger.Verbose("Got {count} work orders from db.", workOrders.Count);
            Logger.Verbose("Caching work orders.");

            await _cachePrimer.PrimeAsync(workOrders);

            Logger.Verbose("Cached all work orders.");
        }
    }
}