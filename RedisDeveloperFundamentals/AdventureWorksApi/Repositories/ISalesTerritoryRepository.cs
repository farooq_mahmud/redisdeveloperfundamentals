﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AdventureWorksApi.Dto;

namespace AdventureWorksApi.Repositories
{
    public interface ISalesTerritoryRepository
    {
        Task<IEnumerable<SalesTerritory>> GetTopSalesTerritoriesAsync(int count);
    }
}