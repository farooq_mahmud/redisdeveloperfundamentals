﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdventureWorksApi.Data;
using AdventureWorksApi.Dto;
using AdventureWorksApi.Extensions;
using Serilog;
using StackExchange.Redis.Extensions.Core;

namespace AdventureWorksApi.Repositories
{
    public sealed class ProductReviewRepository : AdventureWorksRepository, IProductReviewRepository
    {
        public ProductReviewRepository(
            AdventureWorksContext dataContext,
            ICacheClient cacheClient,
            ILogger logger) : base(dataContext, cacheClient, logger)
        {
        }

        public async Task AddProductReviewsAsync(IEnumerable<ProductReview> productReviews)
        {
            var cacheKey = "productReviews";
            var productReviewList = productReviews.ToList();
            
            DataContext.ProductReviews.AddRange(productReviewList);
            await DataContext.SaveChangesAsync();

            Logger.Debug("Added {count} reviews to database. {reviews}", productReviewList.Count, productReviewList);

            var tuples = productReviewList.ToTuples(r => $"{cacheKey}:{r.ProductId}:{r.EmailAddress}");
            await CacheClient.AddAllAsync(tuples.ToList());

            Logger.Debug("Added {count} reviews to cache. {reviews}", productReviewList.Count, productReviewList);
        }
    }
}