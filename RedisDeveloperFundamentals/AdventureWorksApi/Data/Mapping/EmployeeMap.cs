﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using AdventureWorksApi.Dto;

namespace AdventureWorksApi.Data.Mapping
{
    public sealed class EmployeeMap : EntityTypeConfiguration<Employee>
    {
        public EmployeeMap()
        {
            HasKey(t => t.BusinessEntityId);

            Property(t => t.BusinessEntityId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.NationalIdNumber)
                .IsRequired()
                .HasMaxLength(15);

            Property(t => t.LoginId)
                .IsRequired()
                .HasMaxLength(256);

            Property(t => t.JobTitle)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.MaritalStatus)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            Property(t => t.Gender)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            ToTable("Employee", "HumanResources");
            Property(t => t.BusinessEntityId).HasColumnName("BusinessEntityID");
            Property(t => t.NationalIdNumber).HasColumnName("NationalIDNumber");
            Property(t => t.LoginId).HasColumnName("LoginID");
            Property(t => t.OrganizationLevel).HasColumnName("OrganizationLevel");
            Property(t => t.JobTitle).HasColumnName("JobTitle");
            Property(t => t.BirthDate).HasColumnName("BirthDate");
            Property(t => t.MaritalStatus).HasColumnName("MaritalStatus");
            Property(t => t.Gender).HasColumnName("Gender");
            Property(t => t.HireDate).HasColumnName("HireDate");
            Property(t => t.SalariedFlag).HasColumnName("SalariedFlag");
            Property(t => t.VacationHours).HasColumnName("VacationHours");
            Property(t => t.SickLeaveHours).HasColumnName("SickLeaveHours");
            Property(t => t.CurrentFlag).HasColumnName("CurrentFlag");
            Property(t => t.Rowguid).HasColumnName("rowguid");
            Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
        }
    }
}