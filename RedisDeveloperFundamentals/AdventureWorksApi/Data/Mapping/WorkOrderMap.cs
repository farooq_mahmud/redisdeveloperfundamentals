﻿using System.Data.Entity.ModelConfiguration;
using AdventureWorksApi.Dto;

namespace AdventureWorksApi.Data.Mapping
{
    public class WorkOrderMap : EntityTypeConfiguration<WorkOrder>
    {
        public WorkOrderMap()
        {
            HasKey(t => t.WorkOrderId);

            ToTable("WorkOrder", "Production");
            Property(t => t.WorkOrderId).HasColumnName("WorkOrderID");
            Property(t => t.ProductId).HasColumnName("ProductID");
            Property(t => t.OrderQty).HasColumnName("OrderQty");
            Property(t => t.StockedQty).HasColumnName("StockedQty");
            Property(t => t.ScrappedQty).HasColumnName("ScrappedQty");
            Property(t => t.StartDate).HasColumnName("StartDate");
            Property(t => t.EndDate).HasColumnName("EndDate");
            Property(t => t.DueDate).HasColumnName("DueDate");
            Property(t => t.ScrapReasonId).HasColumnName("ScrapReasonID");
            Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
        }
    }
}