﻿using System.Data.Entity.ModelConfiguration;
using AdventureWorksApi.Dto;

namespace AdventureWorksApi.Data.Mapping
{
    public sealed class SalesTerritoryMap : EntityTypeConfiguration<SalesTerritory>
    {
        public SalesTerritoryMap()
        {
            HasKey(t => t.TerritoryId);

            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.CountryRegionCode)
                .IsRequired()
                .HasMaxLength(3);

            Property(t => t.Group)
                .IsRequired()
                .HasMaxLength(50);

            ToTable("SalesTerritory", "Sales");
            Property(t => t.TerritoryId).HasColumnName("TerritoryID");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.CountryRegionCode).HasColumnName("CountryRegionCode");
            Property(t => t.Group).HasColumnName("Group");
            Property(t => t.SalesYtd).HasColumnName("SalesYTD");
            Property(t => t.SalesLastYear).HasColumnName("SalesLastYear");
            Property(t => t.CostYtd).HasColumnName("CostYTD");
            Property(t => t.CostLastYear).HasColumnName("CostLastYear");
            Property(t => t.Rowguid).HasColumnName("rowguid");
            Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
        }
    }
}