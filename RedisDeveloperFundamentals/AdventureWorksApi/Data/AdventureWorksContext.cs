﻿using System.Data.Entity;
using AdventureWorksApi.Data.Mapping;
using AdventureWorksApi.Dto;

namespace AdventureWorksApi.Data
{
    public sealed class AdventureWorksContext : DbContext
    {
        static AdventureWorksContext()
        {
            Database.SetInitializer<AdventureWorksContext>(null);
        }

        public AdventureWorksContext()
            : base("awdb")
        {
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<ProductReview> ProductReviews { get; set; }
        public DbSet<SalesTerritory> SalesTerritories { get; set; }
        public DbSet<WorkOrder> WorkOrders { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new EmployeeMap());
            modelBuilder.Configurations.Add(new ProductReviewMap());
            modelBuilder.Configurations.Add(new SalesTerritoryMap());
            modelBuilder.Configurations.Add(new WorkOrderMap());
        }
    }
}