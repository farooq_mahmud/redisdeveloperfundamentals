﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using AdventureWorksApi.Dto;
using AdventureWorksApi.Models;
using AdventureWorksApi.Repositories;
using AutoMapper;

namespace AdventureWorksApi.Controllers
{
    public class ProductReviewController : ApiControllerBase
    {
        private readonly IProductReviewRepository _productReviewRepository;

        public ProductReviewController(
            IProductReviewRepository productReviewRepository,
            IMapper mapper) : base(mapper)
        {
            _productReviewRepository = productReviewRepository;
        }

        [Route("api/productReviews")]
        public async Task<IHttpActionResult> Post([FromBody] IEnumerable<ProductReviewViewModel> productReviewViewModels)
        {
            var productReviews = Mapper.Map<IEnumerable<ProductReview>>(productReviewViewModels);
            await _productReviewRepository.AddProductReviewsAsync(productReviews);
            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}