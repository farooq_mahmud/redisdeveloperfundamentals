﻿using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using AdventureWorksApi.Models;
using AdventureWorksApi.Repositories;
using AutoMapper;

namespace AdventureWorksApi.Controllers
{
    public class WorkOrdersController : ApiControllerBase
    {
        private readonly IWorkOrderRepository _workOrderRepository;

        public WorkOrdersController(
            IWorkOrderRepository workOrderRepository,
            IMapper mapper) : base(mapper)
        {
            _workOrderRepository = workOrderRepository;
        }

        [Route("api/workOrders/{workOrderId}")]
        public async Task<IHttpActionResult> Get(int workOrderId)
        {
            var workOrder = await _workOrderRepository.GetWorkOrderAsync(workOrderId);
            var workOrderViewModel = Mapper.Map<WorkOrderViewModel>(workOrder);
            return Ok(workOrderViewModel);
        }

        [Route("api/workOrders/prime")]
        public async Task<IHttpActionResult> PrimeCache()
        {
            await _workOrderRepository.PrimeCacheAsync();
            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}