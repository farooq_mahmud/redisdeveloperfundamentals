﻿using System.Web.Http;
using AutoMapper;

namespace AdventureWorksApi.Controllers
{
    public abstract class ApiControllerBase : ApiController
    {
        protected ApiControllerBase(
            IMapper mapper)
        {
            Mapper = mapper;
        }

        protected IMapper Mapper { get; private set; }
    }
}