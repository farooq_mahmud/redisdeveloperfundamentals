﻿using System.Threading.Tasks;
using System.Web.Mvc;
using StackExchange.Redis.Extensions.Core;

namespace AdventureWorksApi.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICacheClient _cacheClient;

        public HomeController(ICacheClient cacheClient)
        {
            _cacheClient = cacheClient;
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public async Task<ActionResult> RedisInfo()
        {
            var info = await _cacheClient.GetInfoAsync();
            return View(info);
        }

        public async Task<ActionResult> Flush()
        {
            await _cacheClient.FlushDbAsync();
            return RedirectToAction("RedisInfo", "Home");
        }
    }
}