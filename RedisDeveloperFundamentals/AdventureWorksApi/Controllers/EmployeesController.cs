﻿using System.Threading.Tasks;
using System.Web.Http;
using AdventureWorksApi.Models;
using AdventureWorksApi.Repositories;
using AutoMapper;

namespace AdventureWorksApi.Controllers
{
    public class EmployeesController : ApiControllerBase
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeesController(
            IEmployeeRepository employeeRepository,
            IMapper mapper) : base(mapper)
        {
            _employeeRepository = employeeRepository;
        }

        [Route("api/employees/{domain}/{userName}")]
        public async Task<IHttpActionResult> Get(string domain, string userName)
        {
            var loginName = $"{domain}\\{userName}";
            var employee = await _employeeRepository.GetEmployeeAsync(loginName);
            var employeeViewModel = Mapper.Map<EmployeeViewModel>(employee);
            return Ok(employeeViewModel);
        }
    }
}