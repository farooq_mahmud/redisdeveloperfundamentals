﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using AdventureWorksApi.Models;
using AdventureWorksApi.Repositories;
using AutoMapper;

namespace AdventureWorksApi.Controllers
{
    public class SalesTerritoriesController : ApiControllerBase
    {
        private readonly ISalesTerritoryRepository _salesTerritoryRepository;

        public SalesTerritoriesController(
            ISalesTerritoryRepository salesTerritoryRepository,
            IMapper mapper) : base(mapper)
        {
            _salesTerritoryRepository = salesTerritoryRepository;
        }

        [HttpGet]
        [Route("api/salesTerritories/top/{count}")]
        public async Task<IHttpActionResult> GetTopSalesTerritories([FromUri] int count)
        {
            var salesTerritories = await _salesTerritoryRepository.GetTopSalesTerritoriesAsync(count);
            var salesTerritoryViewModels = Mapper.Map<IEnumerable<SalesTerritoryViewModel>>(salesTerritories);
            return Ok(salesTerritoryViewModels);
        }
    }
}