﻿using System;

namespace AdventureWorksApi.Dto
{
    public sealed class ProductReview
    {
        public ProductReview()
        {
            var now = DateTime.UtcNow;
            ModifiedDate = now;
            ReviewDate = now;
        }

        public int ProductReviewId { get; set; }
        public int ProductId { get; set; }
        public string ReviewerName { get; set; }
        public DateTime ReviewDate { get; set; }
        public string EmailAddress { get; set; }
        public int Rating { get; set; }
        public string Comments { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}