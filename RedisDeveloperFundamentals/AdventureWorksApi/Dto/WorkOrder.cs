﻿using System;
using ProtoBuf;

namespace AdventureWorksApi.Dto
{
    [ProtoContract]
    public sealed class WorkOrder
    {
        [ProtoMember(1)]
        public int WorkOrderId { get; set; }

        [ProtoMember(2)]
        public int ProductId { get; set; }

        [ProtoMember(3)]
        public int OrderQty { get; set; }

        [ProtoMember(4)]
        public int StockedQty { get; set; }

        [ProtoMember(5)]
        public short ScrappedQty { get; set; }

        [ProtoMember(6)]
        public DateTime StartDate { get; set; }

        [ProtoMember(7)]
        public DateTime? EndDate { get; set; }

        [ProtoMember(8)]
        public DateTime DueDate { get; set; }

        [ProtoMember(9)]
        public short? ScrapReasonId { get; set; }

        [ProtoMember(10)]
        public DateTime ModifiedDate { get; set; }
    }
}