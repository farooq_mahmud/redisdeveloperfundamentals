﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventureWorksApi.Extensions
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<Tuple<string, T>> ToTuples<T>(
            this IEnumerable<T> source,
            Func<T, string> keyFactory)
        {
            return source.Select(item => new Tuple<string, T>(keyFactory(item), item));
        }
    }
}