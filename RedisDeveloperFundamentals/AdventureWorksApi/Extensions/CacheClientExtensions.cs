﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AdventureWorksApi.Dto;
using StackExchange.Redis;
using StackExchange.Redis.Extensions.Core;

namespace AdventureWorksApi.Extensions
{
    public static class CacheClientExtensions
    {
        public static SortedSetEntry[] ToSortedSet<T>(
            this ICacheClient cacheClient,
            IEnumerable<T> source,
            Func<T, double> scoreFactory)
        {
            var sortedSetList = new List<SortedSetEntry>();

            foreach (var item in source)
            {
                var sortedSetEntry = new SortedSetEntry(
                    cacheClient.Serializer.Serialize(item),
                    scoreFactory(item));

                sortedSetList.Add(sortedSetEntry);
            }

            return sortedSetList.ToArray();
        }

        public static IEnumerable<T> ToEnumerable<T>(
            this ICacheClient cacheClient,
            SortedSetEntry[] sortedSet)
        {
            foreach (var sortedSetEntry in sortedSet)
            {
                yield return cacheClient.Serializer.Deserialize<T>(sortedSetEntry.Element);
            }
        }

        public static async Task<WorkOrder> FromHashSet(
            this ICacheClient cacheClient,
            string key)
        {
            var result = await cacheClient.HashGetAllAsync<object>(key);
            var productId = Convert.ToInt32(result["productId"]);
            var orderQty = Convert.ToInt32(result["orderQty"]);
            var stockedQty = Convert.ToInt32(result["stockedQty"]);
            var scrappedQty = Convert.ToInt16(result["scrappedQty"]);
            var startDate = Convert.ToDateTime(result["startDate"]);
            
            var workOrder = new WorkOrder
            {
                ProductId = productId,
                OrderQty = orderQty,
                StockedQty = stockedQty,
                ScrappedQty = scrappedQty,
                StartDate = startDate
            };

            if (result["endDate"] != null)
            {
                workOrder.EndDate = Convert.ToDateTime(result["endDate"]);
            }

            return workOrder;
        }
    }
}