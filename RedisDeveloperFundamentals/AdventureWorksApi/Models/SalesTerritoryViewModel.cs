﻿namespace AdventureWorksApi.Models
{
    public sealed class SalesTerritoryViewModel
    {
        public string Name { get; set; }
        public string CountryRegionCode { get; set; }
        public decimal SalesYtd { get; set; }
    }
}