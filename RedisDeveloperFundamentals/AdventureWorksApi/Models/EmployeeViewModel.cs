﻿namespace AdventureWorksApi.Models
{
    public sealed class EmployeeViewModel
    {
        public string LoginId { get; set; }
        public string JobTitle { get; set; }
        public string NationalIdNumber { get; set; }
    }
}