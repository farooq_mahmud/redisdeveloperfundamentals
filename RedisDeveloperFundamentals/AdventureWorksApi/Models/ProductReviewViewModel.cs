﻿namespace AdventureWorksApi.Models
{
    public sealed class ProductReviewViewModel
    {
        public int ProductId { get; set; }
        public string ReviewerName { get; set; }
        public string Comments { get; set; }
        public string EmailAddress { get; set; }
        public int Rating { get; set; }
    }
}