﻿using AdventureWorksApi.Data;
using Autofac;

namespace AdventureWorksApi.Modules
{
    public sealed class DataContextModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(new AdventureWorksContext()).SingleInstance();
        }
    }
}