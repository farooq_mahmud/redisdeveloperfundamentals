﻿using System;
using System.Collections.Generic;
using AdventureWorksApi.Dto;
using AdventureWorksApi.Repositories;
using Autofac;
using StackExchange.Redis.Extensions.Core;

namespace AdventureWorksApi.Modules
{
    public sealed class CacheFactoryModule : Module
    {
        private readonly Func<ICacheClient, string, WorkOrder> _workOrderHashCacheFactory = (cacheClient, key) =>
        {
            var result = cacheClient.HashGetAll<object>(key);
            var productId = Convert.ToInt32(result["productId"]);
            var orderQty = Convert.ToInt32(result["orderQty"]);
            var stockedQty = Convert.ToInt32(result["stockedQty"]);
            var scrappedQty = Convert.ToInt16(result["scrappedQty"]);
            var startDate = Convert.ToDateTime(result["startDate"]);

            var workOrder = new WorkOrder
            {
                ProductId = productId,
                OrderQty = orderQty,
                StockedQty = stockedQty,
                ScrappedQty = scrappedQty,
                StartDate = startDate
            };

            if (result["endDate"] != null)
            {
                workOrder.EndDate = Convert.ToDateTime(result["endDate"]);
            }

            return workOrder;
        };

        private readonly Func<ICacheClient, string, WorkOrder> _workOrderStringCacheFactory =
            (cacheClient, key) => cacheClient.Get<WorkOrder>(key);


        protected override void Load(ContainerBuilder builder)
        {
            var workOrderCacheDataType = WorkOrderCacheDataType.Hash;

            switch (workOrderCacheDataType)
            {
                case WorkOrderCacheDataType.String:
                    builder.RegisterInstance(_workOrderStringCacheFactory).AsSelf()
                        .As<Func<ICacheClient, string, WorkOrder>>()
                        .SingleInstance();

                    builder.RegisterType<WorkOrderStringCachePrimer>().As<ICachePrimer<IEnumerable<WorkOrder>>>().SingleInstance();

                    break;

                default:
                    builder.RegisterInstance(_workOrderHashCacheFactory).AsSelf()
                        .As<Func<ICacheClient, string, WorkOrder>>()
                        .SingleInstance();

                    builder.RegisterType<WorkOrderHashCachePrimer>().As<ICachePrimer<IEnumerable<WorkOrder>>>().SingleInstance();

                    break;
            }
        }

        private enum WorkOrderCacheDataType
        {
            String = 0,
            Hash
        }
    }
}