﻿using AdventureWorksApi.Repositories;
using Autofac;

namespace AdventureWorksApi.Modules
{
    public sealed class RepositoryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<EmployeeRepository>().As<IEmployeeRepository>().SingleInstance();
            builder.RegisterType<ProductReviewRepository>().As<IProductReviewRepository>().SingleInstance();
            builder.RegisterType<SalesTerritoryRepository>().As<ISalesTerritoryRepository>().SingleInstance();
            builder.RegisterType<WorkOrderRepository>().As<IWorkOrderRepository>().SingleInstance();
        }
    }
}