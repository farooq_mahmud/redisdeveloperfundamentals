﻿using Autofac;
using Autofac.Integration.Mvc;

namespace AdventureWorksApi.Modules
{
    public sealed class MvcModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterControllers(typeof(WebApiApplication).Assembly);
        }
    }
}