﻿using AdventureWorksApi.Handlers;
using Autofac;
using Autofac.Integration.WebApi;

namespace AdventureWorksApi.Modules
{
    public sealed class WebApiModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterApiControllers(typeof(WebApiApplication).Assembly);

            builder.RegisterType<RequestTimingHandler>()
                .AsSelf()
                .SingleInstance();
        }
    }
}