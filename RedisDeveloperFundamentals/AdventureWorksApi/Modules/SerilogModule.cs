﻿using Autofac;
using Serilog;
using SerilogWeb.Classic.Enrichers;

namespace AdventureWorksApi.Modules
{
    public sealed class SerilogModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register((c, p) => new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .Enrich.FromLogContext()
                .Enrich.With<HttpRequestIdEnricher>()
                .WriteTo.Trace().CreateLogger()).SingleInstance();
        }
    }
}