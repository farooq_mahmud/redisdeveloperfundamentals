﻿using Autofac;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using StackExchange.Redis;
using StackExchange.Redis.Extensions.Core;
using StackExchange.Redis.Extensions.Jil;
using StackExchange.Redis.Extensions.Newtonsoft;
using StackExchange.Redis.Extensions.Protobuf;

namespace AdventureWorksApi.Modules
{
    internal enum SerializationMethod
    {
        Newtonsoft = 0,
        Jil,
        Protobuf
    }

    public sealed class CacheModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var mux = ConnectionMultiplexer.Connect(
                new ConfigurationOptions
                {
                    EndPoints = {{"127.0.0.1", 6379}},
                    DefaultDatabase = 0,
                    AllowAdmin = true
                });

            var serializationMethod = SerializationMethod.Newtonsoft;
            var serializer = CreateSerializer(serializationMethod);

            var cacheClient = new StackExchangeRedisCacheClient(mux, serializer);
            builder.RegisterInstance(cacheClient).As<ICacheClient>().SingleInstance();
        }

        private static ISerializer CreateSerializer(SerializationMethod serializationMethod)
        {
            ISerializer serializer;

            switch (serializationMethod)
            {
                case SerializationMethod.Jil:
                    serializer = new JilSerializer();
                    break;

                case SerializationMethod.Protobuf:
                    serializer = new ProtobufSerializer();
                    break;

                default:
                    serializer = new NewtonsoftSerializer(
                        new JsonSerializerSettings
                        {
                            ContractResolver = new CamelCasePropertyNamesContractResolver()
                        });
                    break;
            }

            return serializer;
        }
    }
}