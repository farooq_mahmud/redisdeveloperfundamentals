﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Serilog;

namespace AdventureWorksApi.Handlers
{
    public sealed class RequestTimingHandler : DelegatingHandler
    {
        private readonly ILogger _logger;

        public RequestTimingHandler(ILogger logger)
        {
            _logger = logger.ForContext("Api", "AdventureWorks");
        }


        protected override async Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            var operationDescription = $"Request Time for '{request.RequestUri.AbsoluteUri}'";

            using (_logger.BeginTimedOperation(operationDescription))
            {
                return await base.SendAsync(request, cancellationToken);
            }
        }
    }
}