﻿using Autofac;

namespace AdventureWorksApi
{
    public sealed class ContainerFactory
    {
        public static IContainer Create()
        {
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyModules(typeof(WebApiApplication).Assembly);
            var container = builder.Build();
            return container;
        }
    }
}