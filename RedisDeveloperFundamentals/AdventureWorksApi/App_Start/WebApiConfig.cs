﻿using System.Web.Http;
using AdventureWorksApi.Handlers;
using Autofac.Integration.WebApi;
using Newtonsoft.Json.Serialization;

namespace AdventureWorksApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(ContainerFactory.Create());

            var handler = (RequestTimingHandler)config.DependencyResolver.GetService(typeof(RequestTimingHandler));
            config.MessageHandlers.Insert(0, handler);
            
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{id}",
                new {id = RouteParameter.Optional}
                );
            
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}