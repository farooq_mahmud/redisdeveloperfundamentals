﻿using System;
using System.Collections.Generic;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Redis;

namespace BuggyApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var logSinkType = LogSinkType.HyperLogLog;
            ILogger logger;
            var host = "127.0.0.1";
            var port = 6379;
            var db = 0;
            var key = "buggyApp:events:errors";

            switch (logSinkType)
            {
                case LogSinkType.Set:
                    logger = new LoggerConfiguration()
                        .WriteTo.ColoredConsole()
                        .WriteTo.RedisSet(host, port, db, key)
                        .CreateLogger();
                    break;

                default:

                    IDictionary<LogEventLevel, string> eventTypeToHyperLogLogKeyMap = new Dictionary
                        <LogEventLevel, string>
                    {
                        {LogEventLevel.Error, key}
                    };

                    logger = new LoggerConfiguration()
                        .WriteTo.ColoredConsole()
                        .WriteTo.HyperLogLog(host, port, db, eventTypeToHyperLogLogKeyMap)
                        .CreateLogger();
                    break;
            }


            var eventsToWrite = 1000000;

            for (var i = 0; i < eventsToWrite; i++)
            {
                logger.Error("Error {i} {message}", i, Guid.NewGuid().ToString());
            }

            logger.Information("Done");
            Console.ReadLine();
        }

        private enum LogSinkType
        {
            HyperLogLog = 0,
            Set
        }
    }
}