﻿using System;
using System.Collections.Generic;
using Serilog.Core;
using Serilog.Events;
using StackExchange.Redis.Extensions.Core;

namespace Serilog.Sinks.Redis
{
    public sealed class HyperLogLogEventSink : RedisEventSink, ILogEventSink
    {
        private readonly IDictionary<LogEventLevel, string> _eventTypeToHyperLogLogKeyMap;

        public HyperLogLogEventSink(
            ICacheClient cacheClient,
            IDictionary<LogEventLevel, string> eventTypeToHyperLogLogKeyMap) : base(cacheClient)
        {
            _eventTypeToHyperLogLogKeyMap = eventTypeToHyperLogLogKeyMap;
        }

        public void Emit(LogEvent logEvent)
        {
            var level = logEvent.Level;
            string hyperLogLogKey;

            var gotIt = _eventTypeToHyperLogLogKeyMap.TryGetValue(level, out hyperLogLogKey);

            if (!gotIt)
            {
                return;
            }

            var ts = CacheClient.Serializer.Serialize(logEvent.RenderMessage());
            CacheClient.Database.HyperLogLogAdd(hyperLogLogKey, ts);
        }
    }
}