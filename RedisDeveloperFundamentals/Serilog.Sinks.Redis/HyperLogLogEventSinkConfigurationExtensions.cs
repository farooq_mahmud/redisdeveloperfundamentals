﻿using System.Collections.Generic;
using Serilog.Configuration;
using Serilog.Events;
using StackExchange.Redis;
using StackExchange.Redis.Extensions.Core;
using StackExchange.Redis.Extensions.Jil;

namespace Serilog.Sinks.Redis
{
    public static class HyperLogLogEventSinkConfigurationExtensions
    {
        public static LoggerConfiguration HyperLogLog(
            this LoggerSinkConfiguration loggerSinkConfiguration,
            string host,
            int port,
            int defaultDatabase,
            IDictionary<LogEventLevel, string> eventTypeToHyperLogLogKeyMap,
            LogEventLevel restrictedToMinimuLevel = LevelAlias.Minimum)
        {
            var mux = ConnectionMultiplexer.Connect(
                new ConfigurationOptions
                {
                    EndPoints = { { host, port } },
                    DefaultDatabase = defaultDatabase,
                    AllowAdmin = false
                });

            var serializer = new JilSerializer();
            var cacheClient = new StackExchangeRedisCacheClient(mux, serializer);

            return loggerSinkConfiguration.Sink(
                new HyperLogLogEventSink(cacheClient, eventTypeToHyperLogLogKeyMap), 
                restrictedToMinimuLevel);
        }
    }
}