﻿using System;
using Serilog.Core;
using Serilog.Events;
using StackExchange.Redis.Extensions.Core;

namespace Serilog.Sinks.Redis
{
    public sealed class SetEventSink : RedisEventSink, ILogEventSink
    {
        private readonly string _key;

        public SetEventSink(
            ICacheClient cacheClient,
            string key) : base(cacheClient)
        {
            _key = key;
        }

        public void Emit(LogEvent logEvent)
        {
            CacheClient.SetAdd(_key, Guid.NewGuid().ToString());
        }
    }
}