﻿using System.Collections.Generic;
using Serilog.Configuration;
using Serilog.Events;
using StackExchange.Redis;
using StackExchange.Redis.Extensions.Core;
using StackExchange.Redis.Extensions.Jil;

namespace Serilog.Sinks.Redis
{
    public static class SetEventSinkConfigurationExtensions
    {
        public static LoggerConfiguration RedisSet(
            this LoggerSinkConfiguration loggerSinkConfiguration,
            string host,
            int port,
            int defaultDatabase,
            string key,
            LogEventLevel restrictedToMinimuLevel = LevelAlias.Minimum)
        {
            var mux = ConnectionMultiplexer.Connect(
                new ConfigurationOptions
                {
                    EndPoints = { { host, port } },
                    DefaultDatabase = defaultDatabase,
                    AllowAdmin = false
                });

            var serializer = new JilSerializer();
            var cacheClient = new StackExchangeRedisCacheClient(mux, serializer);

            return loggerSinkConfiguration.Sink(
                new SetEventSink(cacheClient, key),
                restrictedToMinimuLevel);
        }
    }
}