﻿using StackExchange.Redis.Extensions.Core;

namespace Serilog.Sinks.Redis
{
    public abstract class RedisEventSink 
    {
        protected ICacheClient CacheClient { get; private set; }

        protected RedisEventSink(ICacheClient cacheClient)
        {
            CacheClient = cacheClient;
        }
    }
}